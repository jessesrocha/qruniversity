package br.upis.tap.qruniversity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	private Button btnCapture; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		btnCapture = (Button) findViewById(R.id.btn_capture);
		btnCapture.setOnClickListener(captureListener);
		
	}
	
	private View.OnClickListener captureListener = new View.OnClickListener() {
		
		public void onClick(View v) {
		   Intent it = new Intent(MainActivity.this, CaptureActivity.class);
		   startActivity(it);
		}
		
	};

}
