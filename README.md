# QRUniversity

#### O que precisa ser entregue:

  * Documento de Visão;
  * Casos de Uso;
  * Diagrama de Sequência;
  * Modelo de Análise;
  * Documento de Arquitetura do Servidor;
  * Documento de Arquitetura do Cliente;
  * Projeto empacotado (.apk);
  * Código fonte zipado.


## Servidor

  * [http://qruniversity.francisco.pro/listarAulas.php](http://qruniversity.francisco.pro/listarAulas.php "Lista de Aulas")
  * [http://qruniversity.francisco.pro/gerarQrCode.php?id=123](http://qruniversity.francisco.pro/gerarQrCode.php?id=123 "Gerar QrCode")
  * [http://qruniversity.francisco.pro/gerarJson.php?id=123](http://qruniversity.francisco.pro/gerarJson.php?id=123 "Requisição Json")


### Configurações do Banco (Windows)

 Colocar no my.ini (ou my.cnf)

  * **lower_case_table_names = 0**


### Configurações da Conexão ao Banco


#### Modelagem
[MySQL Workbench](http://dev.mysql.com/downloads/tools/workbench/ "MySQL Workbench")

#### Administração
[http://mysql.francisco.pro](http://mysql.francisco.pro "Administração do Banco de Dados")

#### Dados de acesso
  * Servidor: **localhost**
  * Banco: **netinhoi_qruniversity**
  * Usuário: **netinhoi_qrunive**
  * Senha: **ZaLuaMmAtZw5**

### Configurações do FTP

#### Cliente
[FileZilla](https://filezilla-project.org/download.php?type=client "FileZilla")

#### Dados de acesso

  * Servidor: **francisco.pro**
  * Porta: **22**
  * Usuário: **qruniversity@francisco.pro**
  * Senha: **bdBLNgd6FR38**